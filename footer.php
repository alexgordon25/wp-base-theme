<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp-base-theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'wp-base-theme' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'wp-base-theme' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'wp-base-theme' ), 'wp-base-theme', '<a href="http://gordonwebstudio.com" rel="designer">Daniel Gordon</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php 
$super_nonce = wp_create_nonce( 'wpbasethemeNonce' );
echo '<script>var superNonce = "' . $super_nonce . '";</script>';
wp_footer(); 
?>

</body>
</html>
