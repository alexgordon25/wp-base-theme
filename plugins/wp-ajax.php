<?php

/**
 * The function the add_action will call.
 */
function super_ajax_callback() {
	$posts_per_page = $_GET['postsPerPage'];
	echo '<p>OLAKEASE</p>';
	check_ajax_referer( 'wpbasethemeNonce', 'nonce' );



	$query = new WP_Query(array(
		'posts_per_page' => $posts_per_page,
		));

	// If we are returning JSON.
	//wp_send_json( $query->posts ); // encodes the array to JSON and die()

	// wp_send_json_success ( $query->posts ); // Would send something like array( 'success' => true, 'data' => $query->posts );
	// wp_send_json_error( $query->posts ); // Would send something like array( 'success' => false, 'data' => $query->posts );

	// If we are returning HTML.
	foreach ( $query->posts as $newpost ) {
		echo '<div>'.$newpost->post_title.'</div>';
	}

	die(); // always die, otherwise WP continues running and returns a 0.
}


/**
 * We add the actions. The `wp_ajax_` prefix should always be there. The rest will be the "action" provided on the GET/POST rquest.
 */
add_action( 'wp_ajax_super_ajax_callback', 'super_ajax_callback' );
add_action( 'wp_ajax_nopriv_super_ajax_callback', 'super_ajax_callback' );

/**
 * Create a JS variable that includes the /wp-admin/admin-ajax.php URL for our JS to call.
 * The filemtime() is used to provide "versioning" to the JS file, and bust possible caches.
 */
function super_scripts() {
	wp_register_script( 'super-ajax', get_stylesheet_directory_uri() . '/js/wp-ajax.js', array('jquery'), filemtime( get_stylesheet_directory() . '/js/wp-ajax.js' ), true );
	wp_enqueue_script( 'super-ajax' );
	wp_localize_script( 'super-ajax', 'superAjax', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'super_scripts', 10 );


// NONCEs are required by the WP standards, so we set them up. They can go in the template, footter, or anything else. We just make it available to the JS global scope for wp-ajax.js to read it. Your implementation may vary.
// $super_nonce = wp_create_nonce( 'wpbasethemeNonce' );

// echo '<script>var superNonce = "' . $super_nonce . '";';