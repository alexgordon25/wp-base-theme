<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wp-base-theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
				// WP_Query arguments
				$args = array (
					'posts_per_page'         => '2',
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();
						
						get_template_part( 'template-parts/content', 'page' );
					}
				} else {
					echo '<p>no post </p>';
				}

				echo '<button class="btn-trigger">LOAD MORE</button>';
				echo '<p> </p>';
				echo '<p> </p>';

				// Restore original Post Data
				wp_reset_postdata();
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
